/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package partitionproblem;

import com.mycompany.fortestcode.*;

/**
 *
 * @author Acer
 */
class Partition {
    
    static void printSeperateArr(int[] arr, int seperateSize ){
        int[] temp = new int[seperateSize];
        for(int i = 0; i < seperateSize; i++){
            temp[i] = arr[0];
            arr = remove(arr, 0);
        }
        printArr(temp);
        printArr(arr);
    }
    
    static int[] swap(int[]arr, int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
        return arr;
    }
    
    static void permute(int[] arr, int b, int seperateSize) {
        for (int i = b; i < arr.length; i++ ){
            arr = swap(arr, i, b);
            permute(arr, b+1, seperateSize);
            arr = swap(arr, b, i);
        }
        if ( b == arr.length-1){
            printSeperateArr(arr, seperateSize);
            System.out.println("");
        }
        
    }

    static int[] remove(int[] arr, int index) {
        int[] tempArr = new int[arr.length - 1];
        if (index == arr.length - 1) {
            for (int i = 0; i < tempArr.length; i++) {
                tempArr[i] = arr[i];
            }
        } else {
            int count = 0;
            for (int i = 0; i < tempArr.length; i++) {
                if (i == index) {
                    count++;
                    tempArr[i] = arr[i + count];
                } else {
                    tempArr[i] = arr[i + count];
                }
            }
        }
        return tempArr;
    }

    static int[] removeIfContain(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                arr = remove(arr, i);
                return arr;
            }
        }
        return arr;
    }

    static void printArr(int[] arr) {
        if ( arr.length > 0 ){
        for (int i = 0; i < arr.length; i++) {
            if (arr.length == 1) {
                System.out.print("[" + arr[i] + "]");
            } else if (i == 0) {
                System.out.print("[" + arr[i] + ", ");
            } else if (i == arr.length - 1) {
                System.out.print(arr[i] + "]");
            } else {
                System.out.print(arr[i] + ", ");
            }
        }
        }
        else {
            System.out.print("[]");
        }
    }

    static int[] add(int[] arr, int value) {
        int[] temp = new int[arr.length + 1];
        for (int i = 0; i < temp.length; i++) {
            if (i + 1 != temp.length) {
                temp[i] = arr[i];
            } else {
                temp[i] = value;
            }
        }
        return temp;
    }

    static boolean getPartitionSubset(int arr[], int n, int sum, int[] collection) {
        if (sum == 0) {
            System.out.print("Can be divided into ");
            printArr(collection);
            System.out.print(" and ");
            for (int i = 0; i < collection.length; i++) {
                arr = removeIfContain(arr, collection[i]);
            }
            printArr(arr);
            System.out.println("");
            return true;
        }

        if (n == 0 && sum != 0) {
            return false;
        }

        if (arr[n - 1] > sum) {
            return getPartitionSubset(arr, n - 1, sum, collection);
        }

        return getPartitionSubset(arr, n - 1, sum, collection) || getPartitionSubset(arr, n - 1, sum - arr[n - 1], add(collection, arr[n - 1]));

    }

    static boolean findPartition(int arr[], int n, int[] collection) {

        int sum = 0;

        for (int i = 0; i < n; i++) {
            sum += arr[i];
        }

        if (sum % 2 != 0) {
            return false;
        }

        return getPartitionSubset(arr, n, sum / 2, collection);
    }

    public static void main(String[] args) {

        // int arr[] = {1, 4, 1, 3, 2, 6, 3, 2,};
        int arr[] = {3, 1, 5, 9, 10, 2};
        //int[] arr = {1,5,11,5};
        System.out.println("All possibles in both array");
        for ( int i = 0; i < arr.length+1; i++){
        permute(arr, 0, i);
        }
        //    int arr[] = {1, 5, 3};
        //   int arr[] = {3, 1, 5, 9, 12};
        //   int arr[] = {1, 5, 11, 5};
        int[] collection = {};
        int n = arr.length;

        if (findPartition(arr, n, collection) == true) {
            System.out.println("Can be divided into two subsets of equal sum");
        } else {
            System.out.println("Can not be divided into two subsets of equal sum");
        }
    }
}
